#Evan Kelly
#September 25, 2015
#Quiz 2 Task 5

def task():
 z = pickAFile()
 pict = makePicture(z)
 pixels = getPixels(pict)
 h = getHeight(pict)
 w = getWidth(pict)
 show(pict)
 for x in range(0,w):
   for y in range(0,h):
     pixel = getPixelAt(pict,x,y)
     b = getBlue(pixel)
     r = getRed(pixel)
     g = getGreen(pixel)
     newVal = ((r+g+b) / 3) + 75
     r = g = b = newVal
     color = makeColor(r,g,b)
     setColor(pixel, color)
     
 repaint(pict)     
 show(pict)
 
task()