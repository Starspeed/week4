#Evan Kelly
#September 24, 2015
#Quiz terrible frog kill me plz

def mirrorHalf():
  p = pickAFile()
  pict = makePicture(p)
  pixels = getPixels(pict)
  h = getHeight(pict)
  w = getWidth(pict)
  for y in range(0,h):
    for x in range(0,w):
      p = getPixel(pict, x, y)
      z = getPixel(pict, x, h - 1 - y/2)
      color = getColor(p)
      setColor(p, getColor(z))
      setColor(z, color)

    repaint(pict)
    show(pict)
      
mirrorHalf()
