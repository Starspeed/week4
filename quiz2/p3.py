#Evan kelly
#September 23, 2015
#Quiz 2 Part 3

def task():
  p = pickAFile()
  pict = makePicture(p)
  pixels = getPixels(pict)
  h = getHeight(pict)
  w = getWidth(pict)
  for x in range(0,w):
    for y in range(0,h):
      value = getPixelAt(pict,x,y)
      b = getBlue(value)
      r = getRed(value)
      g = getGreen(value)
      setBlue(value, b/2)
      setGreen(value, g/2)
      setRed(value, r*2)
  repaint(pict)
  show(pict)    
task()