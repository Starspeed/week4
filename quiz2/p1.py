#Evan Kelly
#September 22, 2015
#Quiz 2 Task 1

def task():
  x = pickAFile()
  pict = makePicture(x)
  pixel = getPixels(pict)
  for b in getPixels(pict):
    value = getBlue(b)
    if(value <= 150):
      setBlue(b, 255)
      setRed(b, 255)
      setGreen(b, 255)
  repaint(pict)
  show(pict)

task()