#Evan Kelly
#September 24, 2015
#Quiz 2 Part 6

def task():
  p = pickAFile()
  pict = makePicture(p)
  pixels = getPixels(pict)
  h = getHeight(pict)
  q = h/2
  w = getWidth(pict)
  for x in range(0,w):
    for y in range(0,q):
      value = getPixelAt(pict,x,y)
      b = getBlue(value)
      r = getRed(value)
      g = getGreen(value)
      setBlue(value, 0)
      setGreen(value, 0)
      setRed(value, 0)
  repaint(pict)
  show(pict)    
task()