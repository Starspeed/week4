#Evan Kelly
#September 23, 2015
#Quiz 2 Task 3

def task():
  def gsn(pic):  
     for px in getPixels(pic):
        level = 255 - int(0.21*getRed(px) + 0.71*getGreen(px) +0.07*getBlue(px))
        color = makeColor(level, level, level)
        setColor(px, color)
  x = pickAFile()
  pic = makePicture(x)
   
  gsn(pic)
  show(pic)
   
   
task()

